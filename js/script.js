//Funciones generales de local storage
function traerLocalStorage(nombreLS){
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray){
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje){
    Swal.fire({
        icon: icono,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
      });
}

function sweetAlertConBtn(icono, titulo, detalle) {
    Swal.fire(
        titulo,
        detalle,
        icono
      );
}

//Hardcode de administrador
const administrador = {
    usuario: "admin",
    pass: "admin"
}

const adminLogueado = false;

//Clase cliente
class cliente{
    constructor(nombre, apellido, tipoSangre, dni, telefono, observaciones, domicilio, email, membresia){
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoSangre=tipoSangre;
        this.dni = dni;
        this.telefono = telefono;
        this.observaciones = observaciones;
        this.domicilio = domicilio;
        this.email = email;
        this.membresia = membresia;
        this.usuario = dni;

        this.id = generarId(20);
        this.pass = generarId(4);
        this.habilitado = true;
    }
}
const usuarioLogueado = false;

function generarId(length) {
    let id = "";
    let characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      id += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return id;
}

//Login
function login(){
    let usuario = document.getElementById("input_usuario");
    let pass = document.getElementById("input_password");

    if(usuario==null){
        sweetAlertSinBtn('error', 'Ingrese un usuario');
    } else if(pass==null){
        sweetAlertSinBtn('error', 'Ingrese una password');
    } else {
        if(usuario=="admin" && pass == "admin"){
            adminLogueado = true;
        } else {
            let listadoClientes = traerLocalStorage('clientes');
            if(!listadoClientes){
                sweetAlertSinBtn('error', 'No hay clientes registrados');
            } else {
                let usuarioExiste = listadoClientes.find(cliente => cliente.usuario == usuario);
                if(!usuarioExiste){
                    sweetAlertSinBtn('error', 'Usuario no registrado');
                } else {
                    let indexUsuario= listadoClientes.findIndex(cliente => cliente.usuario==usuario);
                    if(pass!=listadoClientes[indexUsuario].pass){
                        sweetAlertSinBtn('error', 'Contraseña incorrecta')
                    } else {
                       if(listadoClientes[indexUsuario].pass.length==4){
                           document.getElementById('form_login').innerHTML = `<label for="input_nuevaPass">Nueva Contraseña</label><input type="password" id="input_nuevaPass"><br><button type="submit" id="btn_nuevaPass" onclick="modificarPass()">Cambiar Pass</button>`;
                       } else {
                           usuarioLogueado = true;
                       }
                    }
                }
            }
    
        }
    }


}



